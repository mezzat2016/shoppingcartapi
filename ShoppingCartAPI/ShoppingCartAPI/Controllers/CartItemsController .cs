﻿using ShoppingCartAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ShoppingCartAPI.Controllers
{
    [Authorize]
    [Route("api/cart-items")]
    [ApiController]
    public class CartItemsController : ControllerBase
    {

        private readonly DBContext _context;

        public CartItemsController(DBContext context)
        {
            _context = context;

            if (!_context.CartItems.Any())
            {
                _context.CartItems.AddRange(
                     new CartItem
                     {
                         UniqueID = 1,
                         ProductID = 11,
                         Price = 50,
                         Discount = 3,
                         Quantity = 4
                     },
                    new CartItem
                    {
                        UniqueID = 2,
                        ProductID = 20,
                        Price = 170,
                        Discount = 20,
                        Quantity = 1
                    },
                    new CartItem
                    {
                        UniqueID = 3,
                        ProductID = 17,
                        Price = 35,
                        Discount = 2,
                        Quantity = 3
                    });
                    
                _context.SaveChanges();
            }
        }

        // GET: api/cart-items
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CartItem>>> Get()
        {
            return await _context.CartItems.ToListAsync();
        }

        // GET api/cart-items/1
        [HttpGet("{id}")]
        public async Task<ActionResult<CartItem>> Get(int id)
        {
            var Employee = await _context.CartItems.FindAsync(id);

            if (Employee == null)
            {
                return NotFound();
            }

            return Employee;
        }

        // POST api/cart-items
        [HttpPost]
        public async Task<ActionResult<CartItem>> Post([FromBody] CartItem item)
        {
            _context.CartItems.Add(item);

            await _context.SaveChangesAsync();

            return CreatedAtAction("Get", new { id =item.UniqueID }, item);
        }

        // PUT api/cart-items/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] CartItem item)
        {
            if (id != item.UniqueID)
            {
                return BadRequest();
            }

            var cartItem = await _context.CartItems.FindAsync(id);

            if (cartItem == null)
            {
                return NotFound();
            }

            cartItem.Quantity = item.Quantity;
            cartItem.ProductID = item.ProductID;
            cartItem.Price = item.Price;
            cartItem.Discount = item.Discount;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException) when (!CartItemExist(id))
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var cartItem = await _context.CartItems.FindAsync(id);

            if (cartItem == null)
            {
                return NotFound();
            }

            _context.CartItems.Remove(cartItem);

            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CartItemExist(int id)
        {
            return _context.CartItems.Any(e => e.UniqueID == id);
        }
    }
}
