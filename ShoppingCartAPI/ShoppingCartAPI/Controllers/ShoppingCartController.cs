﻿using ShoppingCartAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ShoppingCartAPI.Controllers
{
    [Authorize]
    [Route("api/shopping-cart")]
    [ApiController]
    public class ShoppingCartController : ControllerBase
    {

        private readonly DBContext _context;

        public ShoppingCartController(DBContext context)
        {
            _context = context;

        }

        // GET: api/cart-items
        [HttpGet]
        public async Task<ActionResult<ShoppingCart>> Get()
        {
            var cartItems = await _context.CartItems.ToListAsync();

            ShoppingCart cart = ShoppingCart.GetShoppingCart(cartItems);

            return cart;
        }

    }
}
