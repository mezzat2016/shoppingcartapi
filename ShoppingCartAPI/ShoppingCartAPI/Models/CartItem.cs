﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ShoppingCartAPI.Models
{
    public partial class CartItem
    {
        public CartItem()
        {
        }
        [Key]
        public int UniqueID { get; set; }
        public int ProductID{ get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public decimal Discount { get; set; }
    }
}
