﻿using System;
using System.Collections.Generic;

namespace ShoppingCartAPI.Models
{
    public partial class ShoppingCart
    {
        private static ShoppingCart _shoppingCart;
        private ShoppingCart()
        {
        }
        public static ShoppingCart GetShoppingCart(List<CartItem> cartItems)
        {
            if (_shoppingCart == null)
                _shoppingCart = new ShoppingCart();

            _shoppingCart.MapCartItems(cartItems);
            
            return _shoppingCart;
        }
        private void MapCartItems(List<CartItem> cartItems)
        {
            _shoppingCart.CartItems = new List<CartItem>();
            cartItems.ForEach(x =>
            {
                _shoppingCart.CartItems.Add(new CartItem
                {
                    Discount = x.Discount,
                    Price = x.Price,
                    ProductID = x.ProductID,
                    Quantity = x.Quantity,
                    UniqueID = x.UniqueID
                });
            });
        }

        public List<CartItem> CartItems{ get; set; }
        public decimal TotalAmount
        {
            get
            {
                decimal amount = 0;
                if (CartItems != null)
                    CartItems.ForEach(x =>
                    {
                        amount += (x.Price - x.Discount) * x.Quantity;
                    });

                return amount;
            }
        }
    }
}
